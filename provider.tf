provider "aws" {
  region     = "us-east-1"
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.21"
    }
  }

}

resource "aws_instance" "gitlab_runner" {

  instance_type = "t2.micro" 
  ami = "ami-0b5eea76982371e91"

  user_data = <<EOF
#!/bin/bash
#!/bin/bash
echo "hello gitlab-runner"
sudo curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh" | sudo bash
sudo yum install gitlab-runner
sudo gitlab-runner -version
sudo gitlab-runner status
sudo gitlab-runner register --url https://gitlab.com/ --registration-token GR1348941U1aWCyXJGBb2YPmv_xme
EOF

  tags = {
    Name = "gitlab_runner"
  }
}
